﻿using NUnit.Framework;
using Assert = UnityEngine.Assertions.Assert;
using System.Collections;
using System.Collections.Generic;


 public class TestMapManager
{
    string mapText = 
@"+-+-+-+
|S#   |
+-+=+ +
| |G| |
+-+-+-+
[S]start
[A]encounter";

    [Test]
    public void testParseMap()
    {
        var mm = new MapManager();
        mm.ParseMap(mapText);
        Assert.AreEqual("W", mm.MapData[0, 0].North, "0,0,North");
        Assert.AreEqual("W", mm.MapData[0, 0].West, "0,0,West");
        Assert.AreEqual("S", mm.MapData[0, 0].Floor, "0,0,Floor");
        Assert.AreEqual("W", mm.MapData[1, 0].North, "1,0,North");
        Assert.AreEqual("D", mm.MapData[1, 0].West, "1,0,West");
        Assert.AreEqual("", mm.MapData[1, 0].Floor, "1,0,Floor");
        Assert.AreEqual("D", mm.MapData[1, 1].North, "1,1,North");
        Assert.AreEqual("W", mm.MapData[1, 1].West, "1,1,West");
        Assert.AreEqual("G", mm.MapData[1, 1].Floor, "1,1,Floor");

        Assert.AreEqual("start", mm.EventList["S"]);
        Assert.AreEqual("encounter", mm.EventList["A"]);
    }
}

