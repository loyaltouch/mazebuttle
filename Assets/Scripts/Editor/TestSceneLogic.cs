﻿using NUnit.Framework;
using Assert = UnityEngine.Assertions.Assert;
using System.Collections;
using System.Collections.Generic;

public class TestSceneLogic
{
    string memberData = @"name,vit,dex,lck,image,won
you,12,6,9,,
スライム,6,4,0,slime,map
ゾンビ,10,4,0,zombie,map
スケルトン,8,6,0,scelton,sword
幽霊,7,6,0,ghost,map
グール,6,5,0,ghoul,ghoul
バブリースライム,6,5,0,bublyslime,map
ジャックランタン,10,4,0,jack,map
ミミック,2,15,0,mimic,map
人魂,5,5,0,greenwisp,chest_emkey
鬼火,6,6,0,redwisp,map
ウィルオウィスプ,7,7,0,bluewisp,map
";

    [SetUp]
    public void setup()
    {

    }

    [Test]
    public void testParseToken()
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(memberData);
        sl.parseTokens("dex you $ 1 1 + + youAtk turn :=");
        Assert.AreEqual("8", sl.GetGameData("turn", "youAtk"));

        sl.parseTokens("スライム :encounter");
        Assert.AreEqual("スライム", sl.GetGameData("enm", "name"));
        sl.parseTokens("dex enm $ 1 1 + + enmAtk turn :=");
        Assert.AreEqual("6", sl.GetGameData("turn", "enmAtk"));
        sl.parseTokens("enm actor turn :=");
        sl.parseTokens("name actor turn $ $ actorName scene :=");
        Assert.AreEqual("スライム", sl.GetGameData("scene", "actorName"));
    }

    [TestCase("you", "1", "3")]
    [TestCase("you", "0", "1")]
    [TestCase("enm", "1", "1")]
    [TestCase("enm", "0", "3")]
    public void testLuckResult(string actor, string luckResult, string damage)
    {
        var sl = new ScenarioLogic();
        sl.SetGameData("turn", "actor", actor);
        sl.SetGameData("scene", "luckResult", luckResult);
        sl.parseTokens("2 actor turn $ enm == luckResult scene $ + % 2 * 1 + damage scene :=");
        Assert.AreEqual(damage, sl.GetGameData("scene", "damage"));
    }

    [Test]
    public void testRandom()
    {
        var sl = new ScenarioLogic();
        for(var i = 0; i < 100; i++)
        {
            var result = int.Parse(sl.parseTokens("6 :random"));
            Assert.IsTrue(result >= 0, "random >= 0");
            Assert.IsTrue(result <= 5, "random <= 5");
        }

        for(var i = 0; i < 3600; i++)
        {
            var result = int.Parse(sl.parseTokens(":2D6"));
            Assert.IsTrue(result >= 2, "2D6 >= 2");
            Assert.IsTrue(result <= 12, "2D6 <= 12");
        }
    }

    [TestCase("2", "you", "0", "0", "1")]
    [TestCase("12", "you", "0", "1", "0")]
    [TestCase("6", "enm", "1", "0", "0")]
    public void testDamage(string selvalue, string target, string wonFlag, string lostFlag, string nextFlag)
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(memberData);
        sl.SetGameData("scene", "selvalue", selvalue);
        sl.parseTokens("スライム :encounter");
        sl.parseTokens("enm actor turn := " + target + " target turn :=");
        sl.parseTokens("selvalue scene $ vit target turn $ $ - vit target turn $ :=");
        sl.parseTokens("0 vit enm $ :le won scene :=");
        sl.parseTokens("0 vit you $ :le lost scene :=");
        Assert.AreEqual(wonFlag, sl.GetGameData("scene", "won"), "wonフラグ");
        Assert.AreEqual(lostFlag, sl.GetGameData("scene", "lost"), "lostフラグ");
        Assert.AreEqual(nextFlag, sl.parseTokens("won scene $ lost scene $ + :not"), "nextフラグ");
    }

    [Test]
    public void testItem()
    {
        var sl = new ScenarioLogic();
        var result = sl.parseTokens("食料 items #");
        Assert.AreEqual("0", result);
        sl.SetGameData("items", "食料", "0");
        result = sl.parseTokens("食料 items #");
        Assert.AreEqual("0", result);
        sl.SetGameData("items", "食料", "1");
        result = sl.parseTokens("食料 items #");
        Assert.AreEqual("1", result);
        sl.SetGameData("items", "食料", "2");
        result = sl.parseTokens("食料 items #");
        Assert.AreEqual("2", result);
    }

    [Test]
    public void testItemSelector()
    {
        var sl = new ScenarioLogic();
        sl.SetGameData("items", "カボチャ", "3");
        sl.SetGameData("items", "食料", "5");
        sl.SetGameData("items", "ランタン", "1");
        sl.SetGameData("items", "福袋", "1");

        sl.parseTokens("SelectItem items :seldata");
        Assert.AreEqual("カボチャ x 3", sl.selectors[0].Label);
        Assert.AreEqual("SelectItem", sl.selectors[0].Scene);
        Assert.AreEqual("カボチャ", sl.selectors[0].Value);
        Assert.AreEqual("ランタン", sl.selectors[1].Label);
        Assert.AreEqual("SelectItem", sl.selectors[1].Scene);
        Assert.AreEqual("ランタン", sl.selectors[1].Value);
    }

    [TestCase("2", "2", "10")]
    [TestCase("0", "0", "12")]
    [TestCase("-1", "0", "12")]
    public void testDamageMacro(string damage, string result, string nowvit)
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(memberData);
        var ret = sl.parseTokens($"{damage} you :damage");
        Assert.AreEqual(result, ret);
        Assert.AreEqual(nowvit, sl.GetGameData("you", "vit"));
    }

    [TestCase("2", "2", "12")]
    [TestCase("0", "0", "10")]
    [TestCase("-1", "0", "10")]
    [TestCase("3", "2", "12")]
    public void testCureMacro(string cure, string result, string nowvit)
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(memberData);
        sl.SetGameData("you", "vit", "10");
        var ret = sl.parseTokens($"{cure} you :cure");
        Assert.AreEqual(result, ret);
        Assert.AreEqual(nowvit, sl.GetGameData("you", "vit"));
    }

    [TestCase("1", "幽霊", "0")]
    [TestCase("0", "バブリースライム", "0")]
    [TestCase("0", "幽霊", "1")]
    public void testEncounter4(string ghoulFlag, string enmName, string result)
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(memberData);
        sl.SetGameData("flags", "ghoul", ghoulFlag);
        sl.SetGameData("enm", "name", enmName);
        var ret = sl.parseTokens("ghoul flags # :not 幽霊 name enm $ == *");
        Assert.AreEqual(result, ret);
    }

    [Test]
    public void testConcat()
    {
        var sl = new ScenarioLogic();
        var ret = sl.parseTokens("aaa bbb :concat");
        Assert.AreEqual("bbbaaa", ret);
    }

    [Test]
    public void testDump()
    {
        var sl = new ScenarioLogic();
        sl.parseTokens("1 2 3 :dump");
        Assert.AreEqual("3 2 1 0", sl.DebugText);
        // dumpしても次の処理に影響させない
        var ret = sl.parseTokens("1 2 3 :dump +");
        Assert.AreEqual("5", ret);
    }

    [Test]
    public void testScenario()
    {
        var sl = new ScenarioLogic();
        sl.SetGameData("items", "エメラルドの鍵", "1");
        sl.SetGameData("scene", "door", "emerald");
        var ret = sl.parseTokens("emerald door scene $ == エメラルドの鍵 items # *");
        Assert.AreEqual("1", ret);
    }
}

