using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class EndingScene : MonoBehaviour
{
    public TextMeshProUGUI messageText;
    public ParticleSystem ps;
    public Image spawn;
    public Image bg;
    public Image theend;
    private int sequence = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            if (sequence == 0)
            {
                sequence = 1;
                messageText.text = "ここはどこだろう";
            }
            else if (sequence == 1)
            {
                sequence = 2;
                messageText.text = "ここはどこだろう\nそして、どこへ行くのだろう";
            }
            else if (sequence == 2)
            {
                sequence = 3;
                messageText.text = "ここはどこだろう\nそして、どこへ行くのだろう\n口の中から入ったのなら、出る場所はどこになるのだろう";
            }
            else if (sequence == 3)
            {
                sequence = 4;
                messageText.text = "";
                ps.gameObject.SetActive(false);
                spawn.gameObject.SetActive(true);
            }
            else if (sequence == 4)
            {
                sequence = 5;
                messageText.text = "気がついたら、元の場所に戻っていた";
                spawn.gameObject.SetActive(false);
                bg.gameObject.SetActive(true);
            }
            else if(sequence == 5)
            {
                sequence = 6;
                theend.gameObject.SetActive(true);
            }
            else if(sequence == 6)
            {
                if(Input.GetKeyDown(KeyCode.Return))
                {
                    SceneManager.LoadScene("title");
                }
            }
        }
    }
}
