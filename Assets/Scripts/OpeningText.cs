using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class OpeningText : MonoBehaviour
{
    // Start is called before the first frame update
    public Image BG;
    public Image Lip;
    public Image Eaten;

    private TextMeshProUGUI messageText;
    private int sequence = 0;
    void Start()
    {
        BG.enabled = true;
        Lip.enabled = false;
        Eaten.enabled = false;

        messageText = GetComponent<TextMeshProUGUI>();
        sequence = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            if(sequence == 0)
            {
                BG.enabled = false;
                Lip.enabled = true;
                messageText.text = "目の前に巨大な口が現れて";
                sequence = 1;
            }
            else if (sequence == 1)
            {
                Lip.enabled = false;
                Eaten.enabled = true;
                messageText.text = "食われた";
                sequence = 2;
            }
            else
            {
                SceneManager.LoadScene("Maze");
            }
        }
    }
}
