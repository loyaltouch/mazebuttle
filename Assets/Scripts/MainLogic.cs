using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MainLogic : MonoBehaviour
{
    public GameObject _parent;

    public TextMeshProUGUI ui_yourName;
    public TextMeshProUGUI ui_yourVitMax;
    public TextMeshProUGUI ui_yourVitNow;
    public TextMeshProUGUI ui_yourDex;
    public TextMeshProUGUI ui_yourLckMax;
    public TextMeshProUGUI ui_yourLckNow;
    public Slider ui_yourVitGuage;
    public Slider ui_yourLckGuage;
    public TextMeshProUGUI ui_yourPoison;

    public TextMeshProUGUI ui_enemyName;
    public TextMeshProUGUI ui_enemyVitMax;
    public TextMeshProUGUI ui_enemyVitNow;
    public TextMeshProUGUI ui_enemyDex;
    public Slider ui_enemyVitGuage;
    public Image ui_enemyImage;

    public Canvas ui_canvas;
    public Image ui_messagePanel;
    public TextMeshProUGUI ui_message;
    public GameObject ui_selectPanel;
    public Image ui_goal;
    public bool Pushed = false;

    public GameObject LoadInput;
    public TMP_InputField LoadInputText;

    public string SaveStateText { get; set; }

    private bool animating = false;
    private bool touched = false;
    private bool slide = false;
    private GameObject wallPrefab;
    private GameObject floorPrefab;
    private GameObject circlePrefab;
    private GameObject doorPrefab;
    private GameObject selbuttonPrefab;

    private ScenarioLogic scenarioLogic = new ScenarioLogic();
    private MapManager mapManager = new MapManager();

    private string sceneMode = "message";

    private int selIndex = 0;
    private List<SelectButton> selButtons = new List<SelectButton>();
    private bool selAnimating = false;
    private List<string> selKeys = new List<string>();
    private float spaceKeyBreak = 0;

    // Start is called before the first frame update
    void Start()
    {
        // Prefabの読み込み
        wallPrefab = Resources.Load<GameObject>("Prefabs/Wall");
        floorPrefab = Resources.Load<GameObject>("Prefabs/Floor");
        circlePrefab = Resources.Load<GameObject>("Prefabs/Circle");
        doorPrefab = Resources.Load<GameObject>("Prefabs/DoorWall");
        selbuttonPrefab = Resources.Load<GameObject>("Prefabs/SelectorButton");

        // 戦闘データの読み込み
        var ta = Resources.Load<TextAsset>("Data/buttledata");
        scenarioLogic.parseMemberData(ta.text);

        // セーブデータの読み込み
        if (SaveStateText != null && SaveStateText != "")
        {
            loadFromSaveState();
            SaveStateText = "";
            updateAll();
            sceneMode = "map";
        }
        else
        {
            ta = Resources.Load<TextAsset>("scenario/events/start");
            scenarioLogic.parseScene(ta.text, "", "");
            scenarioLogic.SetGameData("map", "name", "map1");
            scenarioLogic.SetGameData("map", "direction", "2");
            initMaze();
            updateAll();
        }
        scenarioLogic.SetGameData("map", "face", scenarioLogic.GetGameData("map", "directioin"));
    }
    // Update is called once per frame
    void Update()
    {
        if(spaceKeyBreak > 0)
        {
            spaceKeyBreak -= Time.deltaTime;
        }

        if (sceneMode == "map")
        {
            mapKeyEvent();
        }
        else if(sceneMode == "gameover")
        {
            gameOverKeyEvent();
        }
        else if (sceneMode == "ending")
        {
            endingKeyEvent();
        }
        else
        {
            messageKeyEvent();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "wall")
        {
            touched = true;
        }
        if (other.gameObject.tag == "door" && !Pushed)
        {
            touched = true;
        }
    }

    private void mapKeyEvent()
    {
        if (!animating)
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                slide = true;
                transform.position = new Vector3(transform.position.x, 0.8f, transform.position.z);
            }
            else
            {
                slide = false;
                transform.position = new Vector3(transform.position.x, 1.0f, transform.position.z);
            }

            if (slide)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    StartCoroutine(movePlayer(0));
                }
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    StartCoroutine(movePlayer(3));
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    StartCoroutine(movePlayer(1));
                }
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    StartCoroutine(movePlayer(2));
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    StartCoroutine(movePlayer(0));
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    StartCoroutine(turnPlayer(3));
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    StartCoroutine(turnPlayer(1));
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    StartCoroutine(turnPlayer(2));
                }
                else if (Input.GetKey(KeyCode.Space))
                {
                    if(spaceKeyBreak > 0)
                    {
                        // スペースキー押下抑制
                    }
                    else if (faceTo() == "D")
                    {
                        // 扉
                        Pushed = true;
                        StartCoroutine(movePlayer(0));
                    }
                    else if(faceTo() == "K")
                    {
                        // 鍵
                        var ta = Resources.Load<TextAsset>("scenario/" + mapManager.EventList[":"]);
                        if (ta != null)
                        {
                            scenarioLogic.parseScene(ta.text, "", "");
                            updateAll();
                            refleshButtons();
                            sceneMode = scenarioLogic.Mode;
                        }
                    }
                }
                else if (Input.GetKeyDown(KeyCode.Escape))
                {
                    var ta = Resources.Load<TextAsset>("scenario/menu/mainmenu");
                    if (ta != null)
                    {
                        scenarioLogic.parseScene(ta.text, "", "");
                        updateAll();
                        refleshButtons();
                        sceneMode = scenarioLogic.Mode;
                    }
                }
            }
        }
    }

    private void messageKeyEvent()
    {
        if (!selAnimating)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (selIndex < selButtons.Count - 1)
                {
                    selIndex++;
                    foreach (var selButton in selButtons)
                    {
                        selButton.Slide(1);
                    }
                }
                else
                {
                    // 先頭に戻る
                    foreach (var selButton in selButtons)
                    {
                        selButton.Slide(selIndex * -1);
                    }
                    selIndex = 0;
                }
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (selIndex > 0)
                {
                    selIndex--;
                    foreach (var selButton in selButtons)
                    {
                        selButton.Slide(-1);
                    }
                }
                else
                {
                    // 末尾に進む
                    foreach (var selButton in selButtons)
                    {
                        selButton.Slide(selButtons.Count - 1);
                    }
                    selIndex = selButtons.Count - 1;

                }
            }

            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
            {
                if ((scenarioLogic.GetGameData("scene", "saveState") == "load") && (LoadInputText.text != ""))
                {
                    // セーブデータをロード
                    SaveStateText = LoadInputText.text;
                    loadFromSaveState();
                    SaveStateText = "";
                    scenarioLogic.ClearGameDataKey("scene");
                    scenarioLogic.SetGameData("map", "face", scenarioLogic.GetGameData("map", "directioin"));
                }
                else if (0 <= selIndex && selIndex < scenarioLogic.selectors.Count)
                {
                    // 普通のイベント読みこみ
                    var select = scenarioLogic.selectors[selIndex];
                    var textResource = Resources.Load<TextAsset>("scenario/" + select.Scene);
                    if(textResource != null)
                    {
                        scenarioLogic.parseScene(textResource.text, select.Label, select.Value);
                    }
                    else
                    {
                        Debug.LogError("not found : " + select.Scene);
                    }

                    // デバッグ
                    if(scenarioLogic.DebugText != "")
                    {
                        Debug.Log(scenarioLogic.DebugText);
                    }

                    // 次マップのロードの場合
                    if (scenarioLogic.GetGameData("scene", "loadMap") != "")
                    {
                        initMaze();
                    }
                }

                // メッセージモードからマップモードに遷移する時、スペースキーを抑制しないと勝手に扉を押し初める
                if((sceneMode != "map") && (scenarioLogic.Mode == "map"))
                {
                    spaceKeyBreak = 0.25f;
                }
                sceneMode = scenarioLogic.Mode;
                updateAll();
                refleshButtons();
            }
        }
    }

    private void gameOverKeyEvent()
    {
        if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("title");
        }
    }

    private void endingKeyEvent()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("ending");
        }
    }

    private IEnumerator turnPlayer(int rotate)
    {
        var amount = (1 - (rotate + 5) % 4) * 90;
        var oldRotate = transform.localRotation;
        var newRotate = oldRotate * Quaternion.AngleAxis(amount, Vector3.down);

        animating = true;
        for (float t = 0; t < 0.25f; t += Time.deltaTime)
        {
            var rotation = Quaternion.Slerp(oldRotate, newRotate, t * 4);
            transform.localRotation = rotation;
            yield return null;
        }
        transform.localRotation = newRotate;
        animating = false;

        var directionIndex = amount / 90;
        var nowIndex = scenarioLogic.GetGameDataInt("map", "direction");
        nowIndex += directionIndex + 4;
        nowIndex %= 4;
        scenarioLogic.SetGameData("map", "direction", nowIndex.ToString());
    }

    private string faceTo()
    {
        var direction = scenarioLogic.GetGameDataInt("map", "direction");
        var mapX = scenarioLogic.GetGameDataInt("map", "x");
        var mapY = scenarioLogic.GetGameDataInt("map", "y");
        if(direction == 0)
        {
            return mapManager.MapData[mapX, mapY].North;
        }
        else if (direction == 1)
        {
            return mapManager.MapData[mapX, mapY].West;
        }
        else if (direction == 2)
        {
            return mapManager.MapData[mapX, mapY + 1].North;
        }
        else if (direction == 3)
        {
            return mapManager.MapData[mapX + 1, mapY].West;
        }
        return "";
    }

    private IEnumerator movePlayer(int trans)
    {
        var face = trans % 4;
        var amount = 1 - ((face / 2) * 2);
        var oldPos = transform.position;
        var newPos = oldPos + transform.forward * 4 * amount;
        if (face % 2 == 1)
        {
            newPos = oldPos + transform.right * 4 * amount;
        }

        animating = true;
        for (float t = 0; t < 0.25f; t += Time.deltaTime)
        {
            var pos = Vector3.Lerp(oldPos, newPos, t * 4);
            transform.position = pos;
            if (touched)
            {
                transform.position = oldPos;
                animating = false;
                touched = false;
                Pushed = false;
                yield break;
            }
            yield return null;
        }
        transform.position = newPos;
        animating = false;

        fixPosition();
        checkEvent(scenarioLogic.GetGameDataInt("map", "x"), scenarioLogic.GetGameDataInt("map", "y"));
        sceneMode = scenarioLogic.Mode;

        Pushed = false;
        scenarioLogic.SetGameData("map", "face", face.ToString());
    }

    private IEnumerator damageEffect(TextMeshProUGUI target)
    {
        var oldPos = target.transform.position;
        target.transform.position = new Vector3(oldPos.x - 10, oldPos.y - 2, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x + 10, oldPos.y - 4, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x - 10, oldPos.y - 6, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x + 10, oldPos.y - 8, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x, oldPos.y, oldPos.z);
    }

    private void refleshButtons()
    {
        foreach (var selButton in selButtons)
        {
            Destroy(selButton.gameObject);
        }
        selButtons = new List<SelectButton>();
        selKeys = new List<string>();
        int i = 0;
        foreach (var select in scenarioLogic.selectors)
        {
            var selButton = Instantiate<GameObject>(selbuttonPrefab, new Vector3(250, 400 - i * 40, 0), Quaternion.identity, ui_canvas.transform);
            var buttonCompo = selButton.GetComponent<SelectButton>();
            buttonCompo.Text = select.Label;
            buttonCompo.FixPosition(i, 0);
            selButton.transform.SetParent(ui_selectPanel.transform);
            selButtons.Add(buttonCompo);
            selKeys.Add(select.Label);
            i++;
        }
        selIndex = 0;
    }

    private void checkEvent(int x, int y)
    {
        var eventKey = mapManager.MapData[x, y].Floor;
        var eventName = "";
        TextAsset ta = null;
        if (mapManager.EventList.TryGetValue(eventKey, out eventName))
        {
            // マップイベント
            ta = Resources.Load<TextAsset>("scenario/" + eventName);
        }
        else
        {
            // 毒
            if(scenarioLogic.GetGameData("you", "poison") == "1")
            {
                ta = Resources.Load<TextAsset>("scenario/poison");
            }
        }

        if (ta != null)
        {
            scenarioLogic.parseScene(ta.text, "", "");
            updateAll();
            refleshButtons();
        }
    }

    private void initMaze()
    {
        var ta = Resources.Load<TextAsset>("data/" + scenarioLogic.GetGameData("map", "name"));
        if(ta == null)
        {
            return;
        }
        mapManager.ParseMap(ta.text);

        ui_goal.gameObject.SetActive(false);
        clearMap();

        for (int i = 0; i < mapManager.XMax; i++)
        {
            for (int j = 0; j < mapManager.YMax; j++)
            {
                if (mapManager.MapData[i, j].North == "W")
                {
                    Instantiate(wallPrefab, new Vector3(i * -4, 2, j * 4 - 2), Quaternion.identity, _parent.transform);
                }
                if (mapManager.MapData[i, j].West == "W")
                {
                    Instantiate(wallPrefab, new Vector3((i * -4) + 2, 2, j * 4), Quaternion.Euler(0, 90.0f, 0), _parent.transform);
                }
                if (mapManager.MapData[i, j].North == "D")
                {
                    Instantiate(doorPrefab, new Vector3(i * -4, 0, j * 4 - 2), Quaternion.identity, _parent.transform);
                }
                if (mapManager.MapData[i, j].West == "D")
                {
                    Instantiate(doorPrefab, new Vector3((i * -4) + 2, 0, j * 4), Quaternion.Euler(0, 90.0f, 0), _parent.transform);
                }
                if (mapManager.MapData[i, j].North == "K")
                {
                    Instantiate(doorPrefab, new Vector3(i * -4, 0, j * 4 - 2), Quaternion.identity, _parent.transform);
                }
                if (mapManager.MapData[i, j].West == "K")
                {
                    Instantiate(doorPrefab, new Vector3((i * -4) + 2, 0, j * 4), Quaternion.Euler(0, 90.0f, 0), _parent.transform);
                }
                if (mapManager.MapData[i, j].Floor == "S")
                {
                    // ゲームデータのX座標・Y座標に何も入っていない場合に限り、スタート地点に移動
                    if(scenarioLogic.GetGameData("map", "x") == "")
                    {
                        scenarioLogic.SetGameData("map", "x", i.ToString());
                    }
                    if (scenarioLogic.GetGameData("map", "y") == "")
                    {
                        scenarioLogic.SetGameData("map", "y", j.ToString());
                    }
                }
                if (mapManager.MapData[i, j].Floor == "G")
                {
                    Instantiate(circlePrefab, new Vector3(i * -4, 0, j * 4), Quaternion.Euler(90.0f, 0, 0), _parent.transform);
                }
                Instantiate(floorPrefab, new Vector3(i * -4, -0.125f, j * 4), Quaternion.identity, _parent.transform);
            }
        }
        warp(scenarioLogic.GetGameDataInt("map", "x"), scenarioLogic.GetGameDataInt("map", "y"));
    }

    private void fixPosition()
    {
        var mapX = (int)((transform.position.x * -1 + 0.5f) / 4);
        var mapY = (int)((transform.position.z + 0.5f) / 4);

        scenarioLogic.SetGameData("map", "x", mapX.ToString());
        scenarioLogic.SetGameData("map", "y", mapY.ToString());
    }

    private void warp(int x, int y)
    {
        transform.position = new Vector3(x * -4, transform.position.y, y * 4);
        fixPosition();
    }

    private void fixDirection()
    {
        var direction = scenarioLogic.GetGameDataInt("map", "direction");
        if (direction == 0)
        {
            transform.forward = new Vector3(0, 0, -1);
        }
        else if (direction == 1)
        {
            transform.forward = new Vector3(1, 0, 0);
        }
        else if (direction == 2)
        {
            transform.forward = new Vector3(0, 0, 1);
        }
        else if (direction == 3)
        {
            transform.forward = new Vector3(-1, 0, 0);
        }
    }


    private void clearMap()
    {
        foreach (Transform childTrans in _parent.transform)
        {
            GameObject.Destroy(childTrans.gameObject);
        }
    }


    private void loadFromSaveState()
    {
        scenarioLogic.SetSaveStateSafe(SaveStateText.Replace("\n", ""));
        initMaze();
        warp(scenarioLogic.GetGameDataInt("map", "x"), scenarioLogic.GetGameDataInt("map", "y"));
        fixDirection();
    }

    private void updateAll()
    {
        updateStatus();
        updateMessage();
        updateBadStatus();
        updateSaveWindow();
        doEffect();
        doUpdateImage();
        updateTrans();
    }

    private void updateEnemyImage()
    {
        if (scenarioLogic.GetGameData("enm", "image") == "0")
        {
            ui_enemyImage.gameObject.SetActive(false);
        }
        else
        {
            ui_enemyImage.gameObject.SetActive(true);
            ui_enemyImage.sprite = Resources.Load<Sprite>("Images/" + scenarioLogic.GetGameData("enm", "image"));
        }
    }

    private void updateStatus()
    {
        ui_yourVitMax.text = $"/{scenarioLogic.GetGameData("you", "vitmax"):00}";
        ui_yourVitNow.text = $"{scenarioLogic.GetGameData("you", "vit"):00}";
        ui_yourDex.text = $"{scenarioLogic.GetGameData("you", "dex"):00}";
        ui_yourLckMax.text = $"/{scenarioLogic.GetGameData("you", "lckmax"):00}";
        ui_yourLckNow.text = $"{scenarioLogic.GetGameData("you", "lck"):00}";
        ui_yourVitGuage.maxValue = scenarioLogic.GetGameDataInt("you", "vitmax");
        ui_yourVitGuage.value = scenarioLogic.GetGameDataInt("you", "vit");
        ui_yourLckGuage.maxValue = scenarioLogic.GetGameDataInt("you", "lckmax"); 
        ui_yourLckGuage.value = scenarioLogic.GetGameDataInt("you", "lck");

        if (scenarioLogic.GetGameData("enm", "name") == "0")
        {
            ui_enemyName.gameObject.SetActive(false);
            updateEnemyImage();
        }
        else
        {
            ui_enemyName.gameObject.SetActive(true);
            ui_enemyName.text = scenarioLogic.GetGameData("enm", "name");
            ui_enemyVitMax.text = $"/{scenarioLogic.GetGameData("enm", "vitmax"):00}";
            ui_enemyVitNow.text = $"{scenarioLogic.GetGameData("enm", "vit"):00}";
            ui_enemyDex.text = $"{scenarioLogic.GetGameData("enm", "dex"):00}";
            ui_enemyVitGuage.maxValue = scenarioLogic.GetGameDataInt("enm", "vitmax");
            ui_enemyVitGuage.value = scenarioLogic.GetGameDataInt("enm", "vit");

            if (scenarioLogic.GetGameData("scene", "damage") == "you")
            {
                StartCoroutine(damageEffect(ui_yourName));
            }
            if (scenarioLogic.GetGameData("scene", "damage") == "enm")
            {
                StartCoroutine(damageEffect(ui_enemyName));
            }
        }
    }

    private void updateSaveWindow()
    {
        if(scenarioLogic.GetGameData("scene", "saveState") == "save")
        {
            LoadInput.SetActive(true);
            LoadInputText.text = scenarioLogic.GetSaveState();
        }
        else if(scenarioLogic.GetGameData("scene", "saveState") == "load")
        {
            LoadInputText.text = "";
            LoadInput.SetActive(true);
        }
        else
        {
            LoadInput.SetActive(false);
        }
    }

    private void updateMessage()
    {
        if(scenarioLogic.Message == "")
        {
            ui_messagePanel.gameObject.SetActive(false);
        }
        else
        {
            ui_messagePanel.gameObject.SetActive(true);
            ui_message.text = scenarioLogic.Message;
        }
    }

    private void doEffect()
    {
        ui_goal.gameObject.SetActive(scenarioLogic.GetGameData("scene", "goalEffect") != "");
        var dEffect = scenarioLogic.GetGameData("scene", "damageEffect");
        if(dEffect == "you")
        {
            StartCoroutine(damageEffect(ui_yourName));
        }
        if (dEffect == "enm")
        {
            StartCoroutine(damageEffect(ui_enemyName));
        }
    }

    private void doUpdateImage()
    {
        if(scenarioLogic.GetGameDataInt("scene", "updateImage") > 0)
        {
            updateEnemyImage();
        }
    }

    private void updateTrans()
    {
        var trans = scenarioLogic.GetGameDataInt("scene", "face");
        if (trans != 0)
        {
            Pushed = true;
            StartCoroutine(movePlayer(trans));
        }
    }

    private void updateBadStatus()
    {
        if(scenarioLogic.GetGameDataInt("you", "poison") == 1)
        {
            ui_yourPoison.text = "毒";
        }
        else
        {
            ui_yourPoison.text = "";
        }
    }
}
