using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class MapManager
{
    private MapDatum[,] mapData;
    public MapDatum[,] MapData { get { return mapData; } }

    private Dictionary<string, string> eventList = new Dictionary<string, string>();
    public Dictionary<string, string> EventList { get { return eventList; } }

    public int XMax { get; set; }
    public int YMax { get; set; }
    string Name { get; set; }


    public void ParseMap(string mapText)
    {
        var result = new Dictionary<string, string>();

        XMax = 0;
        YMax = 0;
        
        var lines = mapText.Split('\n');
        for (var i = 0; i < lines.Length; i++)
        {
            var tokens = lines[i].ToCharArray();
            if(tokens.Length > 0 && tokens[0] == '[')
            {
                // event section
                var key = lines[i].Substring(1, 1);
                var value = lines[i].Replace("\n", "").Replace("\r", "").Substring(3);
                eventList[key] = value;
            }
            else
            {
                // map section
                for (var j = 0; j < tokens.Length; j++)
                {
                    if (tokens[j] == '\n')
                    {

                    }
                    else if (tokens[j] == '-')
                    {
                        result[$"{j / 2}.{i / 2}.nWall"] = "W";
                    }
                    else if (tokens[j] == '=')
                    {
                        result[$"{j / 2}.{i / 2}.nWall"] = "D";
                    }
                    else if (tokens[j] == ':')
                    {
                        result[$"{j / 2}.{i / 2}.nWall"] = "K";
                    }
                    else if (tokens[j] == '|')
                    {
                        result[$"{j / 2}.{i / 2}.wWall"] = "W";
                    }
                    else if (tokens[j] == '#')
                    {
                        result[$"{j / 2}.{i / 2}.wWall"] = "D";
                    }
                    else if (tokens[j] == '"')
                    {
                        result[$"{j / 2}.{i / 2}.wWall"] = "K";
                    }
                    else if ('A' <= tokens[j] && tokens[j] <= 'Z')
                    {
                        result[$"{j / 2}.{i / 2}.floor"] = tokens[j].ToString();
                    }

                    XMax = Math.Max(XMax, j / 2 + 1);
                }
                YMax = Math.Max(YMax, i / 2 + 1);
            }
        }

        mapData = new MapDatum[XMax, YMax];

        for(var x = 0; x < XMax; x++)
        {
            for(var y = 0; y < YMax; y++)
            {
                var datum = new MapDatum();
                var tmp = "";
                if(result.TryGetValue($"{x}.{y}.nWall", out tmp))
                {
                    datum.North = tmp;
                }
                if(result.TryGetValue($"{x}.{y}.wWall", out tmp))
                {
                    datum.West = tmp;
                }
                if(result.TryGetValue($"{x}.{y}.floor", out tmp))
                {
                    datum.Floor = tmp;
                }
                mapData[x, y] = datum;
            }
        }
    }
}

public class MapDatum
{
    public MapDatum()
    {
        North = "";
        West = "";
        Floor = "";
    }

    public string North { get; set; }
    public string West { get; set; }
    public string Floor { get; set; }
}
