using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectButton : MonoBehaviour
{
    public int Index { get; set; }
    public int SelIndex { get; set; }
    public string Text { get; set; }
    TextMeshProUGUI childText;

    // Start is called before the first frame update
    void Start()
    {
        childText = GetComponentInChildren<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        float y = transform.position.y;
        float dist = Mathf.Abs(y - 400) / 120;
        if (dist > 1)
        {
            dist = 1;
        }
        GetComponent<Image>().color = new Color(0, 0, 0, 1 - dist);
        if(Index == SelIndex)
        {
            childText.color = Color.white;
        }
        else
        {
            childText.color = new Color(0.5f, 0.5f, 0.5f, 1 - dist);
        }
        childText.text = Text;
    }

    public void FixPosition(int index, int selIndex)
    {
        Index = index;
        SelIndex = selIndex;

        var yPos = SelIndex - Index;
        transform.position = new Vector3(250, 400 + yPos * 40 ,0);
    }

    public void Slide(int direction)
    {
        StartCoroutine(_slide(direction));
    }

    IEnumerator _slide(int direction)
    {
        var oldPos = transform.position;
        var newPos = new Vector3(oldPos.x, oldPos.y + direction * 40, oldPos.z);
        for (float t = 0; t < 0.125f; t += Time.deltaTime)
        {
            transform.position = Vector3.Lerp(oldPos, newPos, t * 8);
            yield return null;
        }
        SelIndex += direction;
        FixPosition(Index, SelIndex);
    }
}
