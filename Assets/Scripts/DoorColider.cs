using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorColider : MonoBehaviour
{
    public GameObject Axis1;
    public GameObject Axis2;
    private bool animating = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!animating)
        {
            var mainLogic = other.gameObject.GetComponent<MainLogic>();

            var heading = mainLogic.transform.position - transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;
            //Debug.Log(direction);

            var nomalDirection = 1;
            if(direction.x > 0.5f)
            {
                nomalDirection = -1;
            }
            if(direction.z < 0.5f)
            {
                nomalDirection = -1;
            }

            if (mainLogic.Pushed)
            {
                StartCoroutine(doorOpen(nomalDirection));
            }
        }
    }

    private IEnumerator doorOpen(int direction)
    {
        animating = true;
        var oldRotate1 = Axis1.transform.localRotation;
        var oldRotate2 = Axis2.transform.localRotation;

        var newRotate1 = oldRotate1 * Quaternion.AngleAxis(90 * direction * -1, Vector3.up);
        var newRotate2 = oldRotate2 * Quaternion.AngleAxis(90 * direction, Vector3.up);

        for (float t = 0; t < 0.25f; t += Time.deltaTime)
        {
            var rotation1 = Quaternion.Slerp(oldRotate1, newRotate1, t * 4);
            var rotation2 = Quaternion.Slerp(oldRotate2, newRotate2, t * 4);
            Axis1.transform.localRotation = rotation1;
            Axis2.transform.localRotation = rotation2;
            yield return null;
        }
        Axis1.transform.localRotation = oldRotate1;
        Axis2.transform.localRotation = oldRotate2;
        animating = false;
    }
}
