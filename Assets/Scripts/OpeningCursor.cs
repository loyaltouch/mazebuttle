using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;


public class OpeningCursor : MonoBehaviour
{
    public GameObject LoadInput;
    public TMP_InputField LoadInputText;

    private bool animating = false;
    private int index = 0;
    private EventSystem eventSystem;

    // Start is called before the first frame update
    void Start()
    {
        eventSystem = EventSystem.current;
    }

    // Update is called once per frame
    void Update()
    {
        if (!animating)
        {
            if(Input.GetKeyDown(KeyCode.DownArrow) && index < 1)
            {
                StartCoroutine(moveCursol(index + 1));
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) && index > 0)
            {
                StartCoroutine(moveCursol(index - 1));
            }
            if(index == 0)
            {
                LoadInput.SetActive(false);
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
                {
                    SceneManager.LoadScene("Opening");
                }
            }
            else if(index == 1)
            {
                LoadInput.SetActive(true);
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    eventSystem.SetSelectedGameObject(LoadInput);
                    if (LoadInputText.text.Length > 0)
                    {
                        SceneManager.sceneLoaded += SceneLoaded;
                        SceneManager.LoadScene("Maze");
                    }
                }
            }
        }
    }

    private IEnumerator moveCursol(int targetIndex)
    {
        var oldPos = transform.localPosition;
        var newPos = transform.localPosition;
        if(targetIndex == 0)
        {
            newPos = new Vector3(-355, -210);
        }
        else if(targetIndex == 1)
        {
            newPos = new Vector3(-335, -270);
        }
        animating = true;
        for (float f = 0; f < 1; f += Time.deltaTime * 8)
        {
            transform.localPosition = Vector3.Lerp(oldPos, newPos, f);
            yield return null;
        }
        transform.localPosition = newPos;
        animating = false;
        index = targetIndex;
    }

    private void SceneLoaded(Scene nextScene, LoadSceneMode mode)
    {
        var objects = nextScene.GetRootGameObjects();
        foreach(var obj in objects)
        {
            if(obj.name == "Player")
            {
                var mainLogic = obj.GetComponent<MainLogic>();
                mainLogic.SaveStateText = LoadInputText.text;
            }
        }
        SceneManager.sceneLoaded -= SceneLoaded;
    }
}
