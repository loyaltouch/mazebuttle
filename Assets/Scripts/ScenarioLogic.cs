using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Text.RegularExpressions;

public class ScenarioLogic
{
    private Dictionary<string, Dictionary<string, string>> gameData = new Dictionary<string, Dictionary<string, string>>();

    private Dictionary<string, Dictionary<string, string>> memberData;
    public Dictionary<string, Dictionary<string, string>> MemberData { get { return memberData; } }

    public List<Selector> selectors = new List<Selector>();

    public string Message { get; set; }

    public string Mode { get; set; }

    public string DebugText { get; set; }

    private Random random;

    public ScenarioLogic()
    {
        Message = "";
        Mode = "message";
        random = new Random();
    }

    public void parseScene(string xmlText, string selLabel, string selValue)
    {
        // 既定のモードはメッセージモード
        Mode = "message";

        // シーンフラグをクリア
        ClearGameDataKey("scene");

        // セレクターをクリア
        selectors.Clear();

        // デバッグ文字列をクリア
        DebugText = "";

        // 直前の情報をシーンセクションの変数に保持
        gameData["scene"]["selected"] = selLabel;
        gameData["scene"]["selvalue"] = parseTokens(selValue);
        gameData["scene"]["prevmessage"] = Message;

        var rootXml = new XmlDocument();
        rootXml.LoadXml(xmlText);
        var sceneXml = rootXml.SelectSingleNode("scene");

        // フラグ処理
        evalText(sceneXml);

        // メッセージ構成
        Message = createMessage(sceneXml);

        // 選択肢の作成
        selectors.AddRange(createSelectors(sceneXml));
    }

    private void evalText(XmlNode sceneNode)
    {
        foreach (XmlNode node in sceneNode.SelectNodes("eval"))
        {
            if (checkCondition(node))
            {
                string value = node.Attributes["text"].Value;
                parseTokens(value);
            }
        }
    }

    private string createMessage(XmlNode sceneNode)
    {
        string result = "";
        foreach (XmlNode node in sceneNode.SelectNodes("message"))
        {
            if (checkCondition(node))
            {
                result += varExpand(node.InnerText);
            }
        }
        return result;
    }

    private List<Selector> createSelectors(XmlNode sceneNode)
    {
        var result = new List<Selector>();
        foreach (XmlNode node in sceneNode.SelectNodes("select"))
        {
            if (checkCondition(node))
            {
                var selector = new Selector();
                selector.Label = node.InnerText;
                selector.Scene = parseTokens(node.Attributes["scene"].Value);
                if (node.Attributes["value"] != null)
                {
                    selector.Value = parseTokens(node.Attributes["value"].Value);
                }
                result.Add(selector);
            }
        }
        return result;
    }

    public string parseTokens(string text)
    {
        var stack = new Stack<string>();
        stack.Push("0");
        foreach (var token in Regex.Split(text, " +"))
        {
            switch(token){
                case "+" :
                    stack.Push((atoi(stack.Pop()) + atoi(stack.Pop())).ToString());
                    break;
                case "-":
                    stack.Push((atoi(stack.Pop()) - atoi(stack.Pop())).ToString());
                    break;
                case "*":
                    stack.Push((atoi(stack.Pop()) * atoi(stack.Pop())).ToString());
                    break;
                case "/":
                    stack.Push((atoi(stack.Pop()) / atoi(stack.Pop())).ToString());
                    break;
                case "%":
                    stack.Push((atoi(stack.Pop()) % atoi(stack.Pop())).ToString());
                    break;
                case "==":
                    stack.Push(stack.Pop() == stack.Pop() ? "1" : "0");
                    break;
                case ":gt":
                    stack.Push(atoi(stack.Pop()) > atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":ge":
                    stack.Push(atoi(stack.Pop()) >= atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":lt":
                    stack.Push(atoi(stack.Pop()) < atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":le":
                    stack.Push(atoi(stack.Pop()) <= atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":not":
                    stack.Push(stack.Pop() == "0" ? "1" : "0");
                    break;
                case ":=":
                    var section = stack.Pop();
                    var key = stack.Pop();
                    var value = stack.Pop();
                    SetGameData(section, key, value);
                    stack.Push(value);
                    break;
                case ":encounter":
                    setupEnemyData(stack.Pop());
                    break;
                case "$":
                    stack.Push(GetGameData(stack.Pop(), stack.Pop()));
                    break;
                case "#":
                    stack.Push(GetGameDataInt(stack.Pop(), stack.Pop()).ToString());
                    break;
                case ":2D6":
                    stack.Push((random.Next(1, 7) + random.Next(1, 7)).ToString());
                    break;
                case ":random":
                    stack.Push(random.Next(atoi(stack.Pop())).ToString());
                    break;
                case ":strlen":
                    stack.Push(stack.Pop().Length.ToString());
                    break;
                case ":concat":
                    stack.Push(stack.Pop() + stack.Pop());
                    break;
                case ":map":
                    ClearGameDataKey("turn");
                    Mode = "map";
                    break;
                case ":loadMap":
                    stack.Push(loadMap(stack.Pop()));
                    break;
                case ":seldata":
                    var localSelector = createSelectorsFromGameData(stack.Pop(), stack.Pop());
                    selectors.AddRange(localSelector);
                    stack.Push(localSelector.Count.ToString());
                    break;
                case ":damage":
                    stack.Push(McrDamage(stack.Pop(), atoi(stack.Pop())).ToString());
                    break;
                case ":cure":
                    stack.Push(McrCure(stack.Pop(), atoi(stack.Pop())).ToString());
                    break;
                case ":gameover":
                    Mode = "gameover";
                    break;
                case ":ending":
                    Mode = "ending";
                    break;
                case ":dump":
                    DebugText = string.Join<string>(" ", stack.ToArray<string>());
                    break;
                default:
                    stack.Push(token);
                break;
            }
        }
        return stack.Pop();
    }

    public void parseMemberData(string csvText)
    {
        var lines = csvText.Split('\n');
        var headers = new List<string>();
        memberData = new Dictionary<string, Dictionary<string, string>>();

        for (var i = 0; i < lines.Length; i++)
        {
            var cols = lines[i].Replace("\r", "").Split(',');
            if (i == 0)
            {
                headers.AddRange(cols);
            }
            else
            {
                var entry = new Dictionary<string, string>();
                for (var j = 0; j < cols.Length; j++)
                {
                    entry[headers[j]] = cols[j];
                }
                memberData[entry["name"]] = entry;
            }
        }
        setupPlayerData();
        setupFellowData();
        gameData["enm"] = new Dictionary<string, string>();
        gameData["enm"]["name"] = "0";
        gameData["enm"]["image"] = "0";
    }

    private void setupPlayerData()
    {
        try
        {
            gameData["you"] = new Dictionary<string, string>();
            gameData["you"]["name"] = "プレイヤー";
            gameData["you"]["vit"] = memberData["you"]["vit"];
            gameData["you"]["dex"] = memberData["you"]["dex"];
            gameData["you"]["lck"] = memberData["you"]["lck"];
            gameData["you"]["vitmax"] = memberData["you"]["vit"];
            gameData["you"]["lckmax"] = memberData["you"]["lck"];
            gameData["you"]["poison"] = "0";        }
        catch (KeyNotFoundException)
        {
        }
    }

    private void setupFellowData()
    {
        try
        {
            gameData["fellow"] = new Dictionary<string, string>();
            gameData["fellow"]["name"] = memberData["グール"]["name"];
            gameData["fellow"]["vit"] = memberData["グール"]["vit"];
            gameData["fellow"]["dex"] = memberData["グール"]["dex"];
            gameData["fellow"]["lck"] = memberData["グール"]["lck"];
            gameData["fellow"]["vitmax"] = memberData["グール"]["vit"];
            gameData["fellow"]["lckmax"] = memberData["グール"]["lck"];
            gameData["fellow"]["poison"] = "0";
        }
        catch (KeyNotFoundException)
        {
        }
    }

    private void setupEnemyData(string name)
    {
        try
        {
            gameData["enm"] = new Dictionary<string, string>();
            gameData["enm"]["name"] = memberData[name]["name"];
            gameData["enm"]["vit"] = memberData[name]["vit"];
            gameData["enm"]["dex"] = memberData[name]["dex"];
            gameData["enm"]["image"] = memberData[name]["image"];
            gameData["enm"]["vitmax"] = memberData[name]["vit"];
            gameData["enm"]["won"] = memberData[name]["won"];
            gameData["enm"]["undead"] = memberData[name]["undead"];
            gameData["scene"]["updateImage"] = "1";
        }
        catch (KeyNotFoundException)
        {
        }
    }

    private List<Selector> createSelectorsFromGameData(string name, string scene)
    {
        var result = new List<Selector>();
        try
        {
            var keys1 = gameData[name].Keys;
            var keyArray = keys1.ToArray<string>();
            var keyList = new List<string>(keyArray);
            keyList.Sort();
            foreach (var key in keyList)
            {
                var selector = new Selector();
                var itemCount = atoi(gameData[name][key]);
                if(itemCount > 0)
                {
                    if(itemCount == 1)
                    {
                        selector.Label = key;
                    }
                    else
                    {
                        selector.Label = $"{key} x {itemCount}";
                    }
                    selector.Scene = scene;
                    selector.Value = key;
                    result.Add(selector);
                }
            }
        }
        catch (KeyNotFoundException)
        {

        }
        return result;
    }

    public string varExpand(string text)
    {
        return Regex.Replace(text, @"\$(.+?)\.(.+?) ", varExpand_func);
    }

    private string varExpand_func(Match match)
    {
        return GetGameData(match.Groups[1].Value, match.Groups[2].Value);
    }

    public string GetGameData(string key, string name)
    {
        try
        {
            if((key == "you") && (name == "dex"))
            {
                if(GetGameData("you", "equip") == "鋼の剣")
                {
                    return (int.Parse(gameData["you"]["dex"]) + 2).ToString();
                }
            }
            if((key == "you") && (name == "vitmax"))
            {
                if (GetGameData("items", "鋼の鎧") == "1")
                {
                    return (int.Parse(gameData["you"]["vitmax"]) + 4).ToString();
                }
            }
            return gameData[key][name];
        }
        catch (KeyNotFoundException)
        {
            return "";
        }
    }

    public int GetGameDataInt(string key, string name)
    {
        try
        {
            return int.Parse(GetGameData(key, name));
        }
        catch
        {
            return 0;
        }

    }

    public void SetGameData(string key, string name, string value)
    {
        if (!gameData.ContainsKey(key))
        {
            gameData[key] = new Dictionary<string, string>();
        }
        gameData[key][name] = value;
    }

    public void ClearGameDataKey(string section)
    {
        gameData[section] = new Dictionary<string, string>();
    }

    private bool checkCondition(XmlNode node)
    {
        if (node.Attributes["if"] == null)
        {
            return true;
        }
        return atoi(parseTokens(node.Attributes["if"].Value)) > 0;
    }

    private string loadMap(string mapName)
    {
        // マップを読みこむようにゲームエンジンに指示
        SetGameData("scene", "loadMap", "1");
        // マップ名をゲームデータにセット
        SetGameData("map", "name", mapName);
        // 座標をリセットし、マップに指定されたスタート地点に移動するようにする
        SetGameData("map", "x", "");
        SetGameData("map", "y", "");
        return mapName;
    }

    private int atoi(string value)
    {
        try
        {
            return int.Parse(value);
        }
        catch
        {
            return 0;
        }
    }

    public int McrDamage(string name, int value)
    {
        var damage = 0;
        if(value > 0)
        {
            damage = value;
        }
        try
        {
            gameData[name]["vit"] = (atoi(gameData[name]["vit"]) - damage).ToString();
        }
        catch (KeyNotFoundException)
        {

        }
        return damage;
    }

    public int McrCure(string name, int value)
    {
        var cure = 0;
        if(value > 0)
        {
            cure = value;
        }
        try
        {
            var vitold = atoi(gameData[name]["vit"]);
            var vitmax = atoi(gameData[name]["vitmax"]);
            var vitnew = vitold + cure;
            if(vitnew > vitmax)
            {
                vitnew = vitmax;
            }
            cure = vitnew - vitold;
            gameData[name]["vit"] = vitnew.ToString();
        }
        catch (KeyNotFoundException)
        {

        }
        return cure;
    }

    public string GetSaveState()
    {
        var resultList = new List<string>();
        resultList.Add(GetGameData("map", "x"));
        resultList.Add(GetGameData("map", "y"));
        resultList.Add(GetGameData("map", "name"));
        resultList.Add(GetGameData("map", "direction"));
        resultList.Add(GetGameData("you", "vit"));
        resultList.Add(GetGameData("you", "lck"));
        resultList.Add(GetGameData("you", "poison"));
        resultList.Add(GetGameData("you", "equip"));

        var itemList = new List<string>();
        var itemValues = new Dictionary<string, string>();
        gameData.TryGetValue("items", out itemValues);
        if (itemValues != null)
        {
            foreach (var key in itemValues.Keys)
            {
                itemList.Add(string.Format("{0}.{1}", key, itemValues[key]));
            }
            resultList.Add(itemList.Count.ToString());
            resultList.AddRange(itemList);
        }
        else
        {
            resultList.Add("0");
        }

        var flagList = new List<string>();
        var flagValues = new Dictionary<string, string>();
        gameData.TryGetValue("flags", out flagValues);
        if (flagValues != null)
        {
            foreach (var key in flagValues.Keys)
            {
                flagList.Add(string.Format("{0}.{1}", key, flagValues[key]));
            }
            resultList.Add(flagList.Count.ToString());
            resultList.AddRange(flagList);
        }
        else
        {
            resultList.Add("0");
        }

        return string.Join(".", resultList);
    }

    public void SetSaveState(string saveState)
    {
        // アイテム、フラグ等をクリア
        ClearGameDataKey("items");
        ClearGameDataKey("flags");

        var stateTokens = saveState.Split('.');
        SetGameData("map", "x", stateTokens[0]);
        SetGameData("map", "y", stateTokens[1]);
        SetGameData("map", "name", stateTokens[2]);
        SetGameData("map", "direction", stateTokens[3]);
        SetGameData("you", "vit", stateTokens[4]);
        SetGameData("you", "lck", stateTokens[5]);
        SetGameData("you", "poison", stateTokens[6]);
        SetGameData("you", "equip", stateTokens[7]);
        var count = 8;
        var itemSize = atoi(stateTokens[count++]);
        for(var i = 0; i < itemSize; i++)
        {
            SetGameData("items", stateTokens[count++], stateTokens[count++]);
        }
        var flagSize = atoi(stateTokens[count++]);
        for(var i = 0; i < flagSize; i++)
        {
            SetGameData("flags", stateTokens[count++], stateTokens[count++]);
        }
        // ロード後は必ずmapモードになる
        Mode = "map";
        // ロード後はメッセージを空にする
        Message = "";
        // ロード後は選択肢をなし
        selectors.Clear();
    }

    public void SetSaveStateSafe(string saveState)
    {
        var rollBackState = GetSaveState();
        try
        {
            SetSaveState(saveState);
        }
        catch
        {
            SetSaveState(rollBackState);
        }
    }
}

public class Selector
{
    public string Label { get; set; }
    public string Scene { get; set; }
    public string Value { get; set; }

    public Selector()
    {
        Label = "";
        Scene = "";
        Value = "";
    }
}